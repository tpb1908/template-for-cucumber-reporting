package com.example.demo;


import static io.cucumber.junit.platform.engine.Constants.GLUE_PROPERTY_NAME;
import static io.cucumber.junit.platform.engine.Constants.PLUGIN_PROPERTY_NAME;

import io.cucumber.junit.CucumberOptions;
import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;

@Suite
@IncludeEngines("cucumber")
@CucumberOptions(
    glue = {"CucumberStepDef"}
//    plugin = {
//        "pretty",
//        "json:target/cucumber-report/cucumber.json",
//        "html:target/cucumber-report/cucumber.html"}
)
@SelectClasspathResource("/cucumber")

@ConfigurationParameter(key = PLUGIN_PROPERTY_NAME, value = "pretty, json:target/cucumber-reports/CucumberTestReport.json")
@ConfigurationParameter(key = GLUE_PROPERTY_NAME, value = "com.example.demo")

public class CucumberIT {

}

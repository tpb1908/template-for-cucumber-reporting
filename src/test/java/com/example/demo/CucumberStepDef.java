package com.example.demo;

import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;

public class CucumberStepDef {

  @Then("{string} equals {string}")
  public void testSame(String first, String second) {
    Assertions.assertEquals(first, second);
  }

}

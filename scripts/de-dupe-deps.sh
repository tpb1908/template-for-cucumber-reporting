# TODO: On CI there will be another directory, so ../ and ../../
# Consider adding a resources directory
# Move this to a cucumber directory and add a JUnit one if useful
dirs=( css fonts images js )
for d in "${dirs[@]}"
do
  # AFAIK we can't do an overwriting merge with mv, so cp and then delete the new copy
  cp -r "cucumber-html-reports/$d" ./
  rm -rf "cucumber-html-reports/$d"
done

find cucumber-html-reports -type f -name '*.html' -print0 | while read -rd $'\0' file
do
   sed -i 's#src="js#src="../js#g; s#href="css#href="../css#g; s#href="images#href="../images#g' "$file"
done